@include('header')

<div class="container">
    <h1>List</h1>
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
    @include('menu')
    <table class="table table-borderless w-100">
        <tr>
            <th>Sl. No.</th>
            <th>Title</th>
            <th>Description</th>
            <th>Actions</th>
        </tr>
        @php
            $i=1;
        @endphp
        @foreach ($list as $item)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->description }}</td>
                <td>
                    <a href="{{ env('APP_URL') }}/edit/{{ $item->id }}">Edit</a>
                    <a href="{{ env('APP_URL') }}/delete/{{ $item->id }}">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
</div>

@include('footer')