</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>



<script type="text/javascript">

    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

    $(document).ready(function(){

        $(document).on('click', '.remove_variant', function(){
            $(this).closest('.one_variant').remove();
        });

        $("#add_variant").click(function(){

            var element_to_add=$("#one_variant").clone();
            $(element_to_add).removeAttr('id');
            $(element_to_add).addClass('one_variant');
            $("#variant_container").append($(element_to_add));
        });
    });
</script>