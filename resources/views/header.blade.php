<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <style type="text/css">
            .menu_link{
                margin:10px;
            }
            .table td, .table th {
                border: none;
            }

            table.table.table-condensed {
                border: 1px solid black;
            }
            #variant_container_dummy{
                display: none;
            }
        </style>
    </head>
    <body>