@include('header')

<div class="container">

    <h1>Add</h1>
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif

    @include('menu')

    <table class="table table-borderless w-100">
        <tr>
            <td></td>
            <td>Title</td>
            <td><input type="text" name="title" id="title"/></td>
        </tr>
        <tr>
            <td></td>
            <td>Description</td>
            <td><input type="text" name="description" id="description"/></td>
        </tr>
        <tr>
            <td colspan='3'>
                <h4>Variants</h4>
                <button type="button" id="add_variant">Add</button>
            </td>
        </tr>
        <tr>
            <td colspan='3'>
                <form id="variant_container">
                </form>
            </td>
        </tr>
        <tr>
            <td colspan='3' id="variant_container_dummy">
                <table class="table table-borderless w-100 table-condensed" id="one_variant">
                    <tr>
                        <td></td>
                        <td>Image</td>
                        <td><input type="file" name="image[]"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Size</td>
                        <td>
                            <select name="size[]">
                                <option value="1">Small</option>
                                <option value="2">Medium</option>
                                <option value="3">Large</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Color</td>
                        <td>
                            <select name="color[]">
                                <option value="1">Violet</option>
                                <option value="2">Indigo</option>
                                <option value="3">Blue</option>
                                <option value="4">Green</option>
                                <option value="5">Yellow</option>
                                <option value="6">Orange</option>
                                <option value="7">Red</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><button class="remove_variant">Remove</button></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td><input type="submit" id="go" name="go" value="Go"/></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>

@include('footer')

<script type="text/javascript">

    $(document).ready(function(){

        $('#go').click(function(){

            var formData = new FormData($("#variant_container")[0]);
            formData.append('title',$('#title').val());
            formData.append('description',$('#description').val());
            formData.append('_token',CSRF_TOKEN);

            $.ajax({
                url: "{{route('store')}}",
                method: 'post',
                data: formData,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    alert("Done");
                    window.location = "{{route('list')}}";
                }
            });

            return false;
        });
    });
</script>