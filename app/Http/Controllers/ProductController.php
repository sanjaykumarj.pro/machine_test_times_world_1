<?php

namespace App\Http\Controllers;

use App\Models\product;
use App\Models\variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=product::all();
        return view('list', compact(['list']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item=product::create([
            'name' => $request->get('title')
            , 'description' => $request->get('description')
        ]);

        for($i=0; $i<sizeof($request->size); $i++)
        {
            $this_variant=variant::create([
                'size' => $request->size[$i]
                , 'color' => $request->color[$i]
                , 'product_id' => $item->id
            ]);
            $this->uploadImage($request->file('image')[$i], $this_variant->id);
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(product $product)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, product $product)
    {
        $item=product::find($request->id);
        return view('edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        product::find($request->id)->update([
            'name' => $request->get('title')
            , 'description' => $request->get('description')
        ]);

        //manage old variants
        if(!empty($request->get('old_size'))) foreach($request->get('old_size') as $old_variant=>$this_size)
        {
            if(!empty($request->get('remove_old_variant')[$old_variant]))
            {
                //removing old variants that are asked for
                variant::destroy($old_variant);
                unlink(public_path('images/'.$old_variant.'.jpg'));
            }
            else
            {
                //change old variants
                variant::find($old_variant)->update([
                    'size' => $this_size
                    , 'color' => $request->get('old_color')[$old_variant]
                ]);
    
                if(isset($request->file('old_image')[$old_variant]))
                {
                    unlink(public_path('images/'.$old_variant.'.jpg'));
                    $this->uploadImage($request->file('old_image')[$old_variant], $old_variant);
                }

            }
        }

        //add new variants
        if(!empty($request->size)) for($i=0; $i<sizeof($request->size); $i++)
        {
            $this_variant=variant::create([
                'size' => $request->size[$i]
                , 'color' => $request->color[$i]
                , 'product_id' => $request->id
            ]);
            $this->uploadImage($request->file('image')[$i], $this_variant->id);
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, product $product)
    {
        product::destroy($request->id);
        unlink(public_path('images/'.$request->id.'.jpg'));
        return redirect()->route('list')->with('success', 'Deleted');
    }

    public function uploadImage($file, $id){

        $filename = $id.'.jpg';
        $location = 'images';

        $asd=$file->move($location,$filename);
     }
}
