<?php

namespace App\Models;

use App\Models\variant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
        , 'description'
    ];

    public $timestamps = false;

    public function variants()
    {
        return $this->hasMany(variant::class, 'product_id');
    }
}
