<?php

namespace App\Models;

use App\Models\product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class variant extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'size'
        , 'color'
        , 'product_id'
    ];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(product::class, 'product_id');
    }
}
