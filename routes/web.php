<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/list', [ProductController::class, 'index'])->name('list');
Route::get('/add', [ProductController::class, 'create'])->name('create');
Route::post('/add', [ProductController::class, 'store'])->name('store');
Route::get('/edit/{id}', [ProductController::class, 'edit']);
Route::post('/edit/{id}', [ProductController::class, 'update'])->name('update');
Route::get('/delete/{id}', [ProductController::class, 'destroy']);
